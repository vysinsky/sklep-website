import { Col, Container, Row } from 'react-bootstrap'
import { useContext } from 'react'
import Link from 'next/link'

import WebsiteContext from '../src/WebsiteContext'

export function Footer() {
  const { contactData } = useContext(WebsiteContext)

  return (
    <footer className="footer">
      <Container className="footer_top">
        <div>
          <Row>
            <Col xl={4} md={6} lg={4}>
              <div className="footer_widget">
                <div className="footer_logo">
                  <Link href="/">
                    <a>
                      <img src={contactData.logoInverted.url} alt="" />
                    </a>
                  </Link>
                </div>
              </div>
            </Col>
            <Col xl={3} md={6} lg={3}>
              <div className="footer_widget">
                <p>
                  {contactData.street} <br />
                  {contactData.postcode}, {contactData.city} <br />
                  <a href={`tel:${contactData.phone}`}>{contactData.phone}</a> <br />
                  <a href={`mailto:${contactData.email}`}>{contactData.email}</a> <br />
                  IČO: {contactData.icoCode}
                </p>
                <div className="social_links">
                  <ul>
                    <li>
                      <a href={contactData.facebookLink}>
                        <i className="ti-facebook" />
                      </a>
                    </li>
                    <li>
                      <a href={contactData.instagramLink}>
                        <i className="fa fa-instagram" />
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </Col>
            {/*<Col xl={2} md={6} lg={2}>
              <div className="footer_widget">
                <h3 className="footer_title">O nás</h3>
                <ul className="links">
                  <li>
                    <Link href="/kontakt">
                      <a>Kontaktujte nás</a>
                    </Link>
                  </li>
                </ul>
              </div>
            </Col>*/}
          </Row>
        </div>
      </Container>
      <div className="copy-right_text">
        <Container>
          <div className="footer_border" />
          <Row>
            <Col xl={12}>
              <p className="copy_right text-center">
                {/* Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. */}
                Copyright &copy;
                {new Date().getFullYear()}
                All rights reserved | This template is made with <i
                  className="fa fa-heart-o"
                  aria-hidden="true"
                /> by{' '}
                <a href="https://colorlib.com" target="_blank">
                  Colorlib
                </a>
                {/* Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --> */}
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    </footer>
  )
}
