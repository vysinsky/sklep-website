import { galleriesListQuery } from '../src/generated/sdk'
import { Image } from 'react-datocms'
import Link from 'next/link'

interface Props {
  galleries: galleriesListQuery
}

const MAX_ROTATE = 3

function getRandomInt(min, max) {
  return Math.random() * (max - min) + min
}

export function Galleries(props: Props) {
  return (
    <div className="row galleries_list mt-30 mb-30">
      {props.galleries.items.map(gallery => (
        <div key={gallery.id} className="col-md-4">
          <Link href="/fotogalerie/[slug]" as={`/fotogalerie/${gallery.slug}`}>
            <a className="gallery-link">
              <div
                className="single-gallery-image-shadow shadow-sm"
                style={{ transform: `rotateZ(${getRandomInt(-MAX_ROTATE, MAX_ROTATE)}deg)` }}
              />
              <div
                className="single-gallery-image-shadow shadow-sm"
                style={{ transform: `rotateZ(${getRandomInt(-MAX_ROTATE, MAX_ROTATE)}deg)` }}
              />
              <div
                className="single-gallery-image-shadow shadow-sm"
                style={{ transform: `rotateZ(${getRandomInt(-MAX_ROTATE, MAX_ROTATE)}deg)` }}
              />
              <div
                className="single-gallery-image-shadow shadow-sm"
                style={{ transform: `rotateZ(${getRandomInt(-MAX_ROTATE, MAX_ROTATE)}deg)` }}
              />
              <Image className="single-gallery-image shadow" data={gallery.images[0].responsiveImage} />
              <div className="single-gallery-name">{gallery.title}</div>
            </a>
          </Link>
        </div>
      ))}
    </div>
  )
}
