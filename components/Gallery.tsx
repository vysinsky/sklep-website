import { Image } from 'react-datocms'
import { useEffect } from 'react'
import Masonry from 'react-masonry-css'

interface Props {
  gallery: any
}

const breakpointColumnsObj = {
  default: 3,
  780: 2,
  500: 1,
}

export function Gallery(props: Props) {
  useEffect(() => {
    // @ts-ignore
    $('.img-pop-up').magnificPopup({
      type: 'image',
      gallery: {
        enabled: true,
      },
    })
  }, [])

  return (
    <div className="row mt-30 mb-30">
      <Masonry breakpointCols={breakpointColumnsObj} className="masonry" columnClassName="masonry-column">
        {props.gallery.images.map(image => (
          <a key={image.id} href={image.url} className="img-pop-up" title={image.title}>
            <Image className="gallery-image shadow" data={image.responsiveImage} />
          </a>
        ))}
      </Masonry>
    </div>
  )
}
