import { useContext } from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import Link from 'next/link'

import WebsiteContext from '../src/WebsiteContext'

export function Header() {
  const context = useContext(WebsiteContext)

  return (
    <header>
      <div className="header-area ">
        <div id="sticky-header" className="main-header-area">
          <Container fluid>
            <div className="header_bottom_border">
              <Row className="align-items-center">
                <Col xl={2} lg={2} md={2} sm={3} xs={3}>
                  <div className="logo">
                    <Link href="/">
                      <a>
                        <img src={context.contactData.logo.url} alt="" />
                      </a>
                    </Link>
                  </div>
                </Col>
                <Col xl={5} lg={5} md={10} sm={9} xs={9}>
                  <div className="main-menu block">
                    <nav>
                      <ul id="navigation">
                        <li>
                          <Link href="/">
                            <a>Domů</a>
                          </Link>
                        </li>
                        {context.menuLinks.map(({ linkText, slug }) => (
                          <li key={slug}>
                            <Link href="/[slug]" as={`/${slug}`}>
                              <a>{linkText}</a>
                            </Link>
                          </li>
                        ))}
                        <li>
                          <Link href="/fotogalerie">
                            <a>Fotogalerie</a>
                          </Link>
                        </li>
                        <li>
                          <Link href="https://sklep.udvouzvirat.cz/">
                            <a style={{ color: "#ef302c" }}>E-shop</a>
                          </Link>
                        </li>
                        {/*<li>
                          <a href="#">
                            blog <i className="ti-angle-down" />
                          </a>
                          <ul className="submenu">
                            <li>
                              <a href="blog.html">blog</a>
                            </li>
                            <li>
                              <a href="single-blog.html">single-blog</a>
                            </li>
                          </ul>
                        </li>*/}
                      </ul>
                    </nav>
                  </div>
                </Col>
                <Col xl={5} lg={5} className="d-none d-lg-block">
                  <div className="social_wrap d-flex align-items-center justify-content-end">
                    <div className="number">
                      <p>
                        <i className="fa fa-phone" />{' '}
                        <a href={`tel:${context.contactData.phone}`}>{context.contactData.phone}</a>
                      </p>
                      <p>
                        <i className="fa fa-envelope" />{' '}
                        <a href={`mailto:${context.contactData.email}`}>{context.contactData.email}</a>
                      </p>
                    </div>
                    <div className="happy_customer">
                      <a href="https://www.konzument.cz" target="_blank" rel="noreferrer noopener">
                        <img src={context.configuration.happyCustomerLogo.url} alt="Spokojený zákazník" />
                      </a>
                    </div>
                    <div className="social_links d-none d-xl-block">
                      <ul>
                        <li>
                          <a href={context.contactData.instagramLink} target="_blank" rel="noreferrer noopener">
                            {' '}
                            <i className="fa fa-instagram" />{' '}
                          </a>
                        </li>
                        <li>
                          <a href={context.contactData.facebookLink} target="_blank" rel="noreferrer noopener">
                            {' '}
                            <i className="fa fa-facebook" />{' '}
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </Col>
              </Row>
            </div>
          </Container>
        </div>
      </div>
    </header>
  )
}
