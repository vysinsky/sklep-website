import { Footer } from './Footer'
import { Header } from './Header'
import { useContext } from 'react'
import WebsiteContext from '../src/WebsiteContext'

export default function Layout({ children }) {
  const websiteContext = useContext(WebsiteContext)

  return (
    <>
      <Header />
      <div className="content_wrapper">{children}</div>
      <Footer />
    </>
  )
}
