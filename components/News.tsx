import { Col, Row } from 'react-bootstrap'
import ReactHtmlParser from 'react-html-parser'

import { newsListQuery } from '../src/generated/sdk'
import Link from 'next/link'

interface Props {
  news: newsListQuery
  displayItemsCount?: number
}

export function News(props: Props) {
  return (
    <div className="news_area">
      <div className="row">
        {props.news.items.map(news => (
          <div key={news.id} className="single_news col-md-12">
            <h4 className="mb-10">
              <Link href="/novinky/[id]" as={`/novinky/${news.id}`}>
                <a>{news.title}</a>
              </Link>
            </h4>
            <p className="small mb-10">Publikováno: {new Date(news._publishedAt).toLocaleDateString()}</p>
            <p className="content">{ReactHtmlParser(news.content)}</p>
          </div>
        ))}
      </div>
      {props.news.itemsCount.count > props.displayItemsCount && (
        <Row className="row">
          <Col lg={12}>
            <div className="text-center pt-5">
              <Link href="/novinky">
                <a className="boxed-btn4">Více novinek</a>
              </Link>
            </div>
          </Col>
        </Row>
      )}
    </div>
  )
}
