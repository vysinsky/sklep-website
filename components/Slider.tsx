import OwlCarousel from 'react-owl-carousel2'
import { Image } from 'react-datocms'

import { sliderOptions } from '../lib/OwlCarousel'
import { slidesListQuery } from '../src/generated/sdk'

interface Props {
  slides: slidesListQuery
}

export function Slider(props: Props) {
  return (
    <div className="slider_area">
      <OwlCarousel options={sliderOptions}>
        {props.slides.items.map(slide => (
          <Image key={slide.id} data={slide.image.responsiveImage} lazyLoad={false} />
        ))}
      </OwlCarousel>
    </div>
  )
}
