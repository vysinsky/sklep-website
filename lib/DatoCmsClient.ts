import { GraphQLClient } from 'graphql-request'
import { getSdk } from '../src/generated/sdk'

const client = new GraphQLClient('https://graphql.datocms.com', {
  headers: {
    authorization: `Bearer ${process.env.DATOCMS_TOKEN}`,
  },
})

export default getSdk(client)
