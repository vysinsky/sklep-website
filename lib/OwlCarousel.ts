const commonOptions = {
  loop: true,
  margin: 0,
  items: 1,
  autoplay: true,
  navText: ['<i class="ti-angle-left"/>', '<i class="ti-angle-right"/>'],
  nav: true,
  dots: false,
  autoplayHoverPause: true,
  animateOut: 'fadeOut',
  animateIn: 'fadeIn',
  responsive: {
    0: {
      items: 1,
      nav: false,
    },
    767: {
      items: 1,
    },
    992: {
      items: 2,
    },
    1200: {
      items: 2,
    },
    1600: {
      items: 3,
    },
  },
}

export const sliderOptions = {
  ...commonOptions,
}

export const testimonialsOptions = {
  ...commonOptions,
  nav: false,
  dots: true,
}
