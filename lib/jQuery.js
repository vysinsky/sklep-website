import $ from 'jquery'

if (typeof window !== 'undefined') {
  window.jQuery = window.$ = $
}

export default $
