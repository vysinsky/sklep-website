require('dotenv').config()

module.exports = {
  env: {
    DATOCMS_TOKEN: process.env.DATOCMS_TOKEN,
    MAPS_API_KEY: process.env.MAPS_API_KEY,
    GOOGLE_ANALYTICS_TRACKING_ID: process.env.GOOGLE_ANALYTICS_TRACKING_ID,
    LOGROCKET_APP_ID: process.env.LOGROCKET_APP_ID,
    FORCE_MAINTENANCE_MODE: process.env.FORCE_MAINTENANCE_MODE,
    FORCE_PRODUCTION: process.env.FORCE_PRODUCTION === '1',
  },
}
