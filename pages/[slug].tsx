import Layout from '../components/Layout'
import WebsiteContext from '../src/WebsiteContext'
import sdk from '../lib/DatoCmsClient'
import { pageQuery, websiteContextQuery } from '../src/generated/sdk'
import prepareArticleContent from '../src/prepareArticleContent'
import Head from 'next/head'
import { Container, Row } from 'react-bootstrap'
import ReactHtmlParser from 'react-html-parser'

export async function getStaticPaths() {
  const { slugs } = await sdk.pagesSlugs()

  return {
    paths: slugs.map(({ slug }) => ({
      params: {
        slug,
      },
    })),
    fallback: false,
  }
}

export async function getStaticProps({ params }): Promise<{ props: Props }> {
  const websiteContext = await sdk.websiteContext()
  const page = await sdk.page({ slug: params.slug })

  page.data.content = prepareArticleContent(page.data.content)

  return {
    props: {
      websiteContext,
      page,
    },
  }
}

interface Props {
  websiteContext: websiteContextQuery
  page: pageQuery
}

export default function DynamicPage({ websiteContext, page }: Props) {
  return (
    <WebsiteContext.Provider value={websiteContext}>
      <Head>
        <title>{page.data.title} | Vinný sklep "U dvou zvířat"</title>
        <link rel="canonical" href={`https://udvouzvirat.cz/${page.data.slug}`} />
      </Head>
      <Layout>
        <div className="page_content">
          <Container>
            <Row>
              <div className="page_heading">
                <h1>{page.data.title}</h1>
              </div>
            </Row>
            <Row>
              <div>{ReactHtmlParser(page.data.content)}</div>
            </Row>
          </Container>
        </div>
      </Layout>
    </WebsiteContext.Provider>
  )
}
