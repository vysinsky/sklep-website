import App from 'next/app'
import Head from 'next/head'
import Router from 'next/router'

import 'react-owl-carousel2/lib/styles.css'

import '../scss/style.scss'
import { initGA, logPageView } from '../lib/GoogleAnalytics'
import { initLogRocket } from '../lib/LogRocket'
import { isProduction } from '../src/utils'

export default class MyApp extends App {
  componentDidMount() {
    if (isProduction(location)) {
      initGA()
      initLogRocket()
      logPageView()
      Router.events.on('routeChangeComplete', logPageView)
    }
  }

  render() {
    const { Component, pageProps } = this.props

    return (
      <>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <title>Vinný sklep "U dvou zvířat"</title>
        </Head>
        <Component {...pageProps} />
      </>
    )
  }
}
