import Document, { Head, Html, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  render() {
    return (
      <Html lang="cs">
        <Head>
          <meta charSet="utf-8" />
          <meta httpEquiv="x-ua-compatible" content="ie=edge" />
          <meta name="description" content="" />
          <meta name="google-site-verification" content="iO-7DpFSP6LQ-93hKUjLW-2Io4u-dj1KPUvCJgNo3d0" />
          <link rel="stylesheet" href="/css/magnific-popup.css" />
          <link rel="stylesheet" href="/css/font-awesome.min.css" />
          <link rel="stylesheet" href="/css/themify-icons.css" />
          <link rel="stylesheet" href="/css/slick.css" />
          <link rel="stylesheet" href="/css/slicknav.css" />
        </Head>
        <body>
          <Main />
          <script src="/js/vendor/modernizr-3.5.0.min.js" />
          <script src="/js/vendor/jquery-1.12.4.min.js" />
          <script src="/js/waypoints.min.js" />
          <script src="/js/jquery.counterup.min.js" />
          <script src="/js/imagesloaded.pkgd.min.js" />
          <script src="/js/jquery.slicknav.min.js" />
          <script src="/js/jquery.magnific-popup.min.js" />
          <script src="/js/slick.min.js" />
          <script src="/js/main.js" />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
