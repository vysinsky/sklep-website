import { Container } from 'react-bootstrap'

import Layout from '../../components/Layout'
import WebsiteContext from '../../src/WebsiteContext'
import sdk from '../../lib/DatoCmsClient'
import { galleriesListQuery, singleGalleryQuery, websiteContextQuery } from '../../src/generated/sdk'
import { Gallery } from '../../components/Gallery'
import { Galleries } from '../../components/Galleries'
import Head from 'next/head'

export async function getStaticPaths() {
  const { slugs } = await sdk.galleriesSlugs()

  return {
    paths: slugs.map(({ slug }) => ({
      params: {
        slug,
      },
    })),
    fallback: false,
  }
}

export async function getStaticProps({ params }): Promise<{ props: Props }> {
  const websiteContext = await sdk.websiteContext()
  const gallery = await sdk.singleGallery({ slug: params.slug })

  return {
    props: {
      websiteContext,
      gallery,
    },
  }
}

interface Props {
  websiteContext: websiteContextQuery
  gallery: singleGalleryQuery
}

export default function IndexPage(props: Props) {
  return (
    <WebsiteContext.Provider value={props.websiteContext}>
      <Head>
        <title>{props.gallery.data.title} | Fotogalerie | Vinný sklep "U dvou zvířat"</title>
        <link rel="canonical" href={`https://udvouzvirat.cz/fotogalerie/${props.gallery.data.slug}`} />
      </Head>
      <Layout>
        <Container>
          <div className="mt-30">
            <h3 className="mb-30 large">Fotogalerie "{props.gallery.data.title}"</h3>
            <Gallery gallery={props.gallery.data} />
          </div>
        </Container>
      </Layout>
    </WebsiteContext.Provider>
  )
}
