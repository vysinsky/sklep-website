import { Container } from 'react-bootstrap'

import Layout from '../../components/Layout'
import WebsiteContext from '../../src/WebsiteContext'
import sdk from '../../lib/DatoCmsClient'
import { galleriesListQuery, websiteContextQuery } from '../../src/generated/sdk'
import { Gallery } from '../../components/Gallery'
import { Galleries } from '../../components/Galleries'
import Head from 'next/head'

export async function getStaticProps(): Promise<{ props: Props }> {
  const websiteContext = await sdk.websiteContext()
  const galleries = await sdk.galleriesList()

  return {
    props: {
      websiteContext,
      galleries,
    },
  }
}

interface Props {
  websiteContext: websiteContextQuery
  galleries: galleriesListQuery
}

export default function IndexPage(props: Props) {
  return (
    <WebsiteContext.Provider value={props.websiteContext}>
      <Head>
        <title>Fotogalerie | Vinný sklep "U dvou zvířat"</title>
        <link rel="canonical" href="https://udvouzvirat.cz/fotogalerie" />
      </Head>
      <Layout>
        <Container>
          <div className="mt-30 gallery-wrapper">
            <h3 className="mb-30 large">Fotogalerie</h3>
            {props.galleries.galleriesCount.count === 1 ? (
              <Gallery gallery={props.galleries.items[0]} />
            ) : (
              <Galleries galleries={props.galleries} />
            )}
          </div>
        </Container>
      </Layout>
    </WebsiteContext.Provider>
  )
}
