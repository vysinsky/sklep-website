import { Container } from 'react-bootstrap'

import Layout from '../components/Layout'
import { Slider } from '../components/Slider'
import { News } from '../components/News'
import WebsiteContext from '../src/WebsiteContext'
import sdk from '../lib/DatoCmsClient'
import { newsListQuery, slidesListQuery, websiteContextQuery } from '../src/generated/sdk'
import Head from 'next/head'

const DISPLAY_NEWS_COUNT = 3

export async function getStaticProps(): Promise<{ props: Props }> {
  const websiteContext = await sdk.websiteContext()
  const slides = await sdk.slidesList()
  const news = await sdk.newsList({ first: DISPLAY_NEWS_COUNT })

  return {
    props: {
      websiteContext,
      slides,
      news,
    },
  }
}

interface Props {
  websiteContext: websiteContextQuery
  slides: slidesListQuery
  news: newsListQuery
}

export default function IndexPage(props: Props) {
  return (
    <WebsiteContext.Provider value={props.websiteContext}>
      <Head>
        <title>Úvod | Vinný sklep "U dvou zvířat"</title>
        <link rel="canonical" href="https://udvouzvirat.cz" />
      </Head>
      <Layout>
        <Slider slides={props.slides} />
        <Container>
          <div className="mt-30">
            <h3 className="mb-30 large">Novinky</h3>
            <News news={props.news} displayItemsCount={DISPLAY_NEWS_COUNT} />
          </div>
        </Container>
      </Layout>
    </WebsiteContext.Provider>
  )
}
