import Layout from '../../components/Layout'
import WebsiteContext from '../../src/WebsiteContext'
import sdk from '../../lib/DatoCmsClient'
import { singleNewsQuery, websiteContextQuery } from '../../src/generated/sdk'
import prepareArticleContent from '../../src/prepareArticleContent'
import Head from 'next/head'

export async function getStaticPaths() {
  const { ids } = await sdk.newsIds()

  return {
    paths: ids.map(({ id }) => ({
      params: {
        id,
      },
    })),
    fallback: false,
  }
}

export async function getStaticProps({ params }): Promise<{ props: Props }> {
  const websiteContext = await sdk.websiteContext()
  const news = await sdk.singleNews({ id: params.id })

  news.data.content = prepareArticleContent(news.data.content)

  return {
    props: {
      websiteContext,
      news,
    },
  }
}

interface Props {
  websiteContext: websiteContextQuery
  news: singleNewsQuery
}

export default function DynamicPage({ websiteContext, news }: Props) {
  return (
    <WebsiteContext.Provider value={websiteContext}>
      <Head>
        <title>{news.data.title} | Novinky | Vinný sklep "U dvou zvířat"</title>
        <link rel="canonical" href={`https://udvouzvirat.cz/novinky/${news.data.id}`} />
      </Head>
      <Layout>
        <div className="page_content">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="page_heading">
                  <h3>{news.data.title}</h3>
                </div>
                <div className="row">
                  <div className="col-lg-12" dangerouslySetInnerHTML={{ __html: news.data.content }} />
                </div>
                <div className="d-flex justify-content-end">
                  <p className="mt-10 font-italic">
                    Publikováno: {new Date(news.data._publishedAt).toLocaleDateString()}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </WebsiteContext.Provider>
  )
}
