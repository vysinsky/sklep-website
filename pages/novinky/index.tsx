import Layout from '../../components/Layout'
import { News } from '../../components/News'
import WebsiteContext from '../../src/WebsiteContext'
import sdk from '../../lib/DatoCmsClient'
import { newsListQuery, websiteContextQuery } from '../../src/generated/sdk'
import { Container } from 'react-bootstrap'
import Head from 'next/head'

export async function getStaticProps(): Promise<{ props: Props }> {
  const websiteContext = await sdk.websiteContext()
  const news = await sdk.newsList()

  return {
    props: {
      websiteContext,
      news,
    },
  }
}

interface Props {
  websiteContext: websiteContextQuery
  news: newsListQuery
}

export default function IndexPage(props: Props) {
  return (
    <WebsiteContext.Provider value={props.websiteContext}>
      <Head>
        <title>Novinky | Vinný sklep "U dvou zvířat"</title>
        <link rel="canonical" href="https://udvouzvirat.cz/novinky" />
      </Head>
      <Layout>
        <div className="page_content">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="page_heading">
                  <h3>Novinky</h3>
                </div>
                <div className="row">
                  <Container>
                    <News news={props.news} />
                  </Container>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </WebsiteContext.Provider>
  )
}
