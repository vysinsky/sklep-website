import { createContext } from 'react'
import { websiteContextQuery } from './generated/sdk'

export default createContext<websiteContextQuery>({
  contactData: {},
  menuLinks: [],
})
