export const isProduction = (location: Location) =>
  process.env.FORCE_PRODUCTION || /^udvouzvirat.cz/i.test(location.hostname)
